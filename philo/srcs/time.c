/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   time.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: psemsari <psemsari@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/05/06 23:16:33 by psemsari          #+#    #+#             */
/*   Updated: 2021/08/01 17:47:07 by psemsari         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "philo.h"

void	init_time(t_time *time)
{
	gettimeofday(&time->start, NULL);
}

unsigned int	get_time(t_time time)
{
	gettimeofday(&time.end, NULL);
	return ((time.end.tv_sec * (unsigned int)1000 \
	+ time.end.tv_usec / (unsigned int)1000) - \
	(time.start.tv_sec * (unsigned int)1000 \
	+ time.start.tv_usec / (unsigned int)1000));
}

void	do_sleep(unsigned int mili_sec)
{
	usleep(mili_sec * (unsigned int)1000);
}
