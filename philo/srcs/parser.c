/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parser.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: psemsari <psemsari@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/07/27 17:17:17 by psemsari          #+#    #+#             */
/*   Updated: 2021/08/01 17:47:07 by psemsari         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "philo.h"

int	is_maxint(char *number)
{
	int		len;

	len = ft_strlen(number);
	if (len > 10)
		return (1);
	else if (len == 10 && ft_strncmp(number, "2147483647", 10) > 0)
		return (1);
	return (0);
}

int	ft_allnumber(char *number)
{
	while (*number)
	{
		if ((int)*number > 47 && (int)*number < 58)
			number++;
		else
			return (0);
	}
	return (1);
}

int	data_verification(int *input, int argc, char **argv)
{
	int	i;

	if (argc < 5 || argc > 6)
		return (-1);
	i = 1;
	while (i < argc)
	{
		if (ft_allnumber(argv[i]) && !is_maxint(argv[i]))
			input[i - 1] = ft_atoi(argv[i]);
		else
			return (-1);
		i++;
	}
	return (0);
}

int	init_input(t_data *data, int argc, char **argv)
{
	int	*input;

	input = malloc(5 * sizeof(int));
	if (input == NULL)
		return (1);
	input[4] = 0;
	if (data_verification(input, argc, argv))
	{
		free(input);
		return (1);
	}
	data->number_philo = input[0];
	data->time_to_die = input[1];
	data->time_to_eat = input[2];
	data->time_to_sleep = input[3];
	data->number_of_time_eat = input[4];
	free(input);
	return (0);
}
