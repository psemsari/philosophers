/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   set_mutex.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: psemsari <psemsari@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/07/29 18:06:10 by psemsari          #+#    #+#             */
/*   Updated: 2021/08/01 17:47:07 by psemsari         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "philo.h"

void	set_eat_x_time(t_philo *philo)
{
	pthread_mutex_lock(philo->mutex_eat_x_time);
	(*(philo->eat_x_time))++;
	pthread_mutex_unlock(philo->mutex_eat_x_time);
}

void	set_philo_stop(t_philo *philo)
{
	pthread_mutex_lock(philo->mutex_stop);
	(*(philo->stop)) = 1;
	pthread_mutex_unlock(philo->mutex_stop);
}

void	set_philo_last_eat(t_philo *philo, unsigned int set)
{
	pthread_mutex_lock(&philo->mutex_last_eat);
	philo->last_eat = set;
	pthread_mutex_unlock(&philo->mutex_last_eat);
}

int	philo_is_a_dead(t_philo	*philo, int forks)
{
	if (get_philo_stop(philo))
	{
		if (forks)
		{
			pthread_mutex_unlock(&philo->mutex_fork[philo->gauche]);
			pthread_mutex_unlock(&philo->mutex_fork[philo->droite]);
		}
		return (1);
	}
	return (0);
}

void	*stop_after_eat_x_time(void *arg)
{
	t_data	*data;
	int		stop;

	data = (t_data *)arg;
	stop = 0;
	while (get_eat_x_time(data) < data->number_philo)
	{
		do_sleep(100);
		pthread_mutex_lock(data->mutex_stop);
		stop = data->stop;
		pthread_mutex_unlock(data->mutex_stop);
		if (stop)
			return (NULL);
	}
	pthread_mutex_lock(data->mutex_stop);
	data->stop = 1;
	pthread_mutex_unlock(data->mutex_stop);
	return (NULL);
}
