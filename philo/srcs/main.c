/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: psemsari <psemsari@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/05/06 14:24:39 by psemsari          #+#    #+#             */
/*   Updated: 2021/08/01 19:22:51 by psemsari         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "philo.h"

int	init_data(t_data *data)
{
	int	i;

	i = 0;
	init_time(&data->time);
	data->stop = 0;
	data->eat_x_time = 0;
	if (alloc_data(data))
	{
		destroy_data(data);
		return (1);
	}
	pthread_mutex_init(data->mutex_stop, NULL);
	pthread_mutex_init(data->mutex_print, NULL);
	pthread_mutex_init(data->mutex_eat_x_time, NULL);
	i = 0;
	while (i < data->number_philo)
	{
		pthread_mutex_init(&data->fourchettes[i], NULL);
		i++;
	}
	return (0);
}

void	wait_philo(t_data *data)
{
	int	i;

	i = 0;
	pthread_join(data->view_x_time_eat, NULL);
	while (i < data->number_philo)
	{
		pthread_join(data->philosophes[i].thread, NULL);
		i++;
	}
	i = 0;
	while (i < data->number_philo)
	{
		pthread_mutex_destroy(&data->fourchettes[i]);
		i++;
	}
	i = 0;
	while (i < data->number_philo)
	{
		pthread_mutex_destroy(&data->philosophes[i].mutex_last_eat);
		i++;
	}
}

void	start_philo(t_data *data)
{
	int	i;

	i = 1;
	while (i <= data->number_philo)
	{
		if (i % 2 == 1)
		{
			init_philo(data, i - 1);
			pthread_create(&data->philosophes[i - 1].thread, NULL, \
						&philo_routine, &data->philosophes[i - 1]);
		}
		i++;
	}
	do_sleep(1);
	i = 1;
	while (i <= data->number_philo)
	{
		if (i % 2 == 0)
		{
			init_philo(data, i - 1);
			pthread_create(&data->philosophes[i - 1].thread, NULL, \
						&philo_routine, &data->philosophes[i - 1]);
		}
		i++;
	}
}

int	main(int argc, char **argv)
{
	t_data		data;

	if (init_input(&data, argc, argv))
		return (1);
	if (init_data(&data))
		return (1);
	start_philo(&data);
	pthread_create(&data.view_x_time_eat, NULL, &stop_after_eat_x_time, &data);
	wait_philo(&data);
	pthread_mutex_destroy(data.mutex_print);
	pthread_mutex_destroy(data.mutex_stop);
	pthread_mutex_destroy(data.mutex_eat_x_time);
	destroy_data(&data);
	return (0);
}
