/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   utils.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: psemsari <psemsari@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/07/29 18:05:04 by psemsari          #+#    #+#             */
/*   Updated: 2021/08/01 17:47:07 by psemsari         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "philo.h"

int	ft_strlen(char	*str)
{
	int	i;

	i = 0;
	while (*str++)
		i++;
	return (i);
}

int	ft_atoi(const char *str)
{
	int	i;
	int	result;
	int	neg;

	i = 0;
	result = 0;
	neg = 1;
	while ((str[i] >= 9 && str[i] <= 13) || str[i] == 32)
		i++;
	if (str[i] == '-' || str[i] == '+')
	{
		if (str[i] == '-')
			neg = -1;
		i++;
	}
	while ((int)str[i] > 47 && (int)str[i] < 58)
	{
		result *= 10;
		result += (int)str[i] - 48;
		i++;
	}
	return (result * neg);
}

int	ft_strncmp(const char *s1, const char *s2, size_t n)
{
	size_t	i;
	int		ret;

	i = 0;
	if (n == 0)
		return (0);
	while (s1[i] == s2[i] && s1[i] != '\0' && s2[i] != '\0' && i < n - 1)
		i++;
	if ((unsigned char)s1[i] - (unsigned char)s2[i] > 0)
		ret = 1;
	else if ((unsigned char)s1[i] - (unsigned char)s2[i] < 0)
		ret = -1;
	else
		ret = 0;
	return (ret);
}

void	*join_heart(pthread_t heart)
{
	pthread_join(heart, NULL);
	return (NULL);
}
