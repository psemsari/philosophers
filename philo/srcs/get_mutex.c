/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_mutex.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: psemsari <psemsari@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/07/29 18:11:33 by psemsari          #+#    #+#             */
/*   Updated: 2021/12/02 10:27:18 by psemsari         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "philo.h"

int	get_eat_x_time(t_data *data)
{
	int		ret;

	ret = 0;
	pthread_mutex_lock(data->mutex_eat_x_time);
	ret = data->eat_x_time;
	pthread_mutex_unlock(data->mutex_eat_x_time);
	return (ret);
}

int	get_philo_stop(t_philo *philo)
{
	int		ret;

	ret = 0;
	pthread_mutex_lock(philo->mutex_stop);
	ret = *(philo->stop);
	pthread_mutex_unlock(philo->mutex_stop);
	return (ret);
}

unsigned int	get_philo_last_eat(t_philo *philo)
{
	unsigned int	ret;

	ret = 0;
	pthread_mutex_lock(&philo->mutex_last_eat);
	ret = get_time(philo->time) - philo->last_eat;
	pthread_mutex_unlock(&philo->mutex_last_eat);
	return (ret);
}

void	printf_secure(char *str, unsigned int time, t_philo *philo)
{
	pthread_mutex_lock(philo->mutex_print);
	printf("%u %d %s\n", time, philo->number, str);
	pthread_mutex_unlock(philo->mutex_print);
}
