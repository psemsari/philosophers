/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   manag_philo.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: psemsari <psemsari@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/07/29 18:07:33 by psemsari          #+#    #+#             */
/*   Updated: 2021/08/01 20:15:32 by psemsari         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "philo.h"

void	*health_care(void *arg)
{
	t_philo			*philo;
	unsigned int	last_meat;

	philo = (t_philo *)arg;
	while (!get_philo_stop(philo))
	{
		last_meat = get_philo_last_eat(philo);
		if (last_meat > (unsigned int)philo->time_to_die)
		{
			set_philo_stop(philo);
			printf_secure("died", get_time(philo->time), philo);
			return (NULL);
		}
		do_sleep(5);
	}
	return (NULL);
}

int	first_philo(t_philo *philo)
{
	pthread_mutex_lock(&philo->mutex_fork[philo->droite]);
	if (philo_is_a_dead(philo, 0))
	{
		pthread_mutex_unlock(&philo->mutex_fork[philo->droite]);
		return (1);
	}
	printf_secure("has taken a fork", get_time(philo->time), philo);
	if (philo->number_philo == 1)
		return (1);
	pthread_mutex_lock(&philo->mutex_fork[philo->gauche]);
	return (0);
}

int	philo_take_forks(t_philo *philo)
{
	if (philo->number == 1)
	{
		if (first_philo(philo))
			return (1);
	}
	else
	{
		pthread_mutex_lock(&philo->mutex_fork[philo->gauche]);
		if (philo_is_a_dead(philo, 0))
		{
			pthread_mutex_unlock(&philo->mutex_fork[philo->gauche]);
			return (1);
		}
		printf_secure("has taken a fork", get_time(philo->time), philo);
		pthread_mutex_lock(&philo->mutex_fork[philo->droite]);
	}
	if (philo_is_a_dead(philo, 1))
		return (1);
	printf_secure("has taken a fork", get_time(philo->time), philo);
	return (0);
}

int	philo_eating(t_philo *philo)
{
	set_philo_last_eat(philo, get_time(philo->time));
	if (philo_is_a_dead(philo, 1))
		return (1);
	printf_secure("is eating", get_time(philo->time), philo);
	do_sleep(philo->time_to_eat);
	pthread_mutex_unlock(&philo->mutex_fork[philo->gauche]);
	pthread_mutex_unlock(&philo->mutex_fork[philo->droite]);
	philo->time_eat++;
	if (philo->time_eat == philo->number_of_time_eat)
		set_eat_x_time(philo);
	return (0);
}

void	*philo_routine(void *arg)
{
	t_philo		*philo;
	pthread_t	heart;

	philo = (t_philo *)arg;
	philo->time_eat = 0;
	pthread_create(&heart, NULL, &health_care, arg);
	while (1)
	{
		if (philo_take_forks(philo))
			return (join_heart(heart));
		if (philo_eating(philo))
			return (join_heart(heart));
		if (philo_is_a_dead(philo, 0))
			return (join_heart(heart));
		printf_secure("is sleeping", get_time(philo->time), philo);
		do_sleep(philo->time_to_sleep);
		if (philo_is_a_dead(philo, 0))
			return (join_heart(heart));
		printf_secure("is thinking", get_time(philo->time), philo);
		if (philo->number_philo % 2 == 1)
			do_sleep(10);
	}
	return (join_heart(heart));
}
