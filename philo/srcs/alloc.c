/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   alloc.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: psemsari <psemsari@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/08/01 16:46:08 by psemsari          #+#    #+#             */
/*   Updated: 2021/08/01 19:19:12 by psemsari         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "philo.h"

void	destroy_data(t_data *data)
{
	if (data->mutex_print)
		free(data->mutex_print);
	if (data->mutex_stop)
		free(data->mutex_stop);
	if (data->mutex_eat_x_time)
		free(data->mutex_eat_x_time);
	if (data->fourchettes)
		free(data->fourchettes);
	if (data->philosophes)
		free(data->philosophes);
}

int	alloc_data(t_data *data)
{
	data->fourchettes = \
		(pthread_mutex_t *)malloc(sizeof(pthread_mutex_t) * data->number_philo);
	if (data->fourchettes == NULL)
		return (1);
	data->mutex_stop = (pthread_mutex_t *)malloc(sizeof(pthread_mutex_t));
	if (data->mutex_stop == NULL)
		return (1);
	data->mutex_eat_x_time = (pthread_mutex_t *)malloc(sizeof(pthread_mutex_t));
	if (data->mutex_eat_x_time == NULL)
		return (1);
	data->mutex_print = (pthread_mutex_t *)malloc(sizeof(pthread_mutex_t));
	if (data->mutex_print == NULL)
		return (1);
	data->philosophes = (t_philo *)malloc(sizeof(t_philo) * data->number_philo);
	if (data->philosophes == NULL)
		return (1);
	return (0);
}

void	init_philo(t_data *data, int number)
{
	t_philo	*philo;

	philo = &data->philosophes[number];
	philo->number_of_time_eat = data->number_of_time_eat;
	philo->time_to_die = data->time_to_die;
	philo->time_to_eat = data->time_to_eat;
	philo->time_to_sleep = data->time_to_sleep;
	philo->number_philo = data->number_philo;
	philo->number = number + 1;
	philo->time = data->time;
	philo->stop = &data->stop;
	philo->last_eat = 0;
	philo->gauche = number;
	if (philo->number == data->number_philo)
		philo->droite = 0;
	else
		philo->droite = number + 1;
	philo->eat_x_time = &data->eat_x_time;
	pthread_mutex_init(&(philo->mutex_last_eat), NULL);
	philo->mutex_stop = data->mutex_stop;
	philo->mutex_fork = data->fourchettes;
	philo->mutex_print = data->mutex_print;
	philo->mutex_eat_x_time = data->mutex_eat_x_time;
}
