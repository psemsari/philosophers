/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   time.h                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: psemsari <psemsari@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/05/06 23:17:24 by psemsari          #+#    #+#             */
/*   Updated: 2021/07/29 17:56:56 by psemsari         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef TIME_H
# define TIME_H

typedef struct s_time
{
	struct timeval	start;
	struct timeval	end;
}				t_time;

void			init_time(t_time *time);
unsigned int	get_time(t_time time);
void			do_sleep(unsigned int mili_sec);
void			ft_sleep(unsigned int milisecond);

#endif
