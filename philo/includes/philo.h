/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   philo.h                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: psemsari <psemsari@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/05/10 16:44:23 by psemsari          #+#    #+#             */
/*   Updated: 2021/08/01 17:46:08 by psemsari         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef PHILO_H
# define PHILO_H

# include <stdlib.h>
# include <unistd.h>
# include <stdio.h>
# include <sys/time.h>
# include <pthread.h>
# include <string.h>
# include "time.h"

typedef struct s_philo
{
	pthread_t		thread;
	int				number;
	t_time			time;

	int				number_of_time_eat;
	int				time_to_die;
	int				time_to_eat;
	int				time_to_sleep;
	int				number_philo;

	int				time_eat;

	int				*stop;
	unsigned int	last_eat;
	int				gauche;
	int				droite;
	int				*eat_x_time;

	pthread_mutex_t	*mutex_fork;
	pthread_mutex_t	*mutex_print;
	pthread_mutex_t	*mutex_stop;
	pthread_mutex_t	mutex_last_eat;
	pthread_mutex_t	*mutex_eat_x_time;
}				t_philo;

typedef struct s_data
{
	int				number_of_time_eat;
	int				time_to_die;
	int				time_to_eat;
	int				time_to_sleep;
	int				number_philo;

	int				stop;
	t_time			time;
	int				eat_x_time;

	t_philo			*philosophes;
	pthread_mutex_t	*fourchettes;

	pthread_mutex_t	*mutex_print;
	pthread_mutex_t	*mutex_stop;
	pthread_mutex_t	*mutex_eat_x_time;

	pthread_t		view_x_time_eat;
}				t_data;

void			destroy_data(t_data *data);
int				alloc_data(t_data *data);
void			init_philo(t_data *data, int number);

int				get_eat_x_time(t_data *data);
int				get_philo_stop(t_philo *philo);
unsigned int	get_philo_last_eat(t_philo *philo);
void			printf_secure(char *str, unsigned int time, t_philo *philo);

int				init_data(t_data *data);
void			wait_philo(t_data *data);
void			start_philo(t_data *data);
int				main(int argc, char **argv);

void			*health_care(void *arg);
int				first_philo(t_philo *philo);
int				philo_take_forks(t_philo *philo);
int				philo_eating(t_philo *philo);
void			*philo_routine(void *arg);

int				is_maxint(char *number);
int				ft_allnumber(char *number);
int				data_verification(int *input, int argc, char **argv);
int				init_input(t_data *data, int argc, char **argv);

void			set_eat_x_time(t_philo *philo);
void			set_philo_stop(t_philo *philo);
void			set_philo_last_eat(t_philo *philo, unsigned int set);
int				philo_is_a_dead(t_philo	*philo, int forks);
void			*stop_after_eat_x_time(void *arg);

void			init_time(t_time *time);
unsigned int	get_time(t_time time);
void			do_sleep(unsigned int mili_sec);

int				ft_strlen(char	*str);
int				ft_atoi(const char *str);
int				ft_strncmp(const char *s1, const char *s2, size_t n);
void			*join_heart(pthread_t heart);

#endif
